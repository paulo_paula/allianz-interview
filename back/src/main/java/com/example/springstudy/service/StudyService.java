package com.example.springstudy.service;

import com.example.springstudy.mockdata.MockData;
import com.example.springstudy.model.Client;
import com.example.springstudy.model.ContractDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudyService {

    @Autowired
    private MockData mockData;

    public List<Client> getAllClients() {
        return mockData.getClients();
    }

    public List<ContractDetail> getContractDetailsByName(String name) {
        return mockData.getDetails().stream()
                .filter(detail -> detail.getClientName().toLowerCase().equals(name.toLowerCase()))
                .collect(Collectors.toList());
    }

}
