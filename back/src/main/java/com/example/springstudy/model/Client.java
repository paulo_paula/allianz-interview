package com.example.springstudy.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Client {

    private String name;
    private int age;
    private String dateOfBirth;
    private long clientPhoneNumber;

    public Client(String name, int age, String dateOfBirth, long clientPhoneNumber) {
        this.name = name;
        this.age = age;
        this.dateOfBirth = dateOfBirth;
        this.clientPhoneNumber = clientPhoneNumber;
    }

}
