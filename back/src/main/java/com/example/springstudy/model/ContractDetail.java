package com.example.springstudy.model;

import lombok.Getter;
import lombok.Setter;
import lombok.val;

@Getter
@Setter
public class ContractDetail {

    private String clientName;
    private long clientPhoneNumber;
    private String contractDate;
    private int value;
    private String endingDate;

    public ContractDetail(String clientName, long clientPhoneNumber, String contractDate, int value, String endingDate) {
        this.clientName = clientName;
        this.clientPhoneNumber = clientPhoneNumber;
        this.contractDate = contractDate;
        this.value = value;
        this.endingDate = endingDate;
    }

}
