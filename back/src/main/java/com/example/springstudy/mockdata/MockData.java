package com.example.springstudy.mockdata;

import com.example.springstudy.model.Client;
import com.example.springstudy.model.ContractDetail;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MockData {

    public List<Client> getClients() {
        List<Client> clients = new ArrayList<>();
        Client client1 = new Client("Paulo Paula", 43, "19/12/1979", 198756748L);
        Client client2 = new Client("João Santos", 24, "21/04/1999", 565748930L);
        Client client3 = new Client("Raquel Lopes", 24, "21/04/1999", 343523456L);
        Client client4 = new Client("Patrícia Ferreira", 24, "21/04/1999", 348948930L);
        Client client5 = new Client("Luís Pereira", 24, "21/04/1999", 565746356L);
        Client client6 = new Client("Samuel Santos", 24, "21/04/1999", 565436830L);
        Client client7 = new Client("Gonçalo Barros", 24, "21/04/1999", 562647930L);
        Client client8 = new Client("Eva Domingues", 24, "21/04/1999", 279048930L);
        Client client9 = new Client("Miguel Lisboa", 24, "21/04/1999", 435648930L);
        clients.add(client1);
        clients.add(client2);
        clients.add(client3);
        clients.add(client4);
        clients.add(client5);
        clients.add(client6);
        clients.add(client7);
        clients.add(client8);
        clients.add(client9);
        return clients;
    }

    public List<ContractDetail> getDetails() {
        List<ContractDetail> details = new ArrayList<>();
        ContractDetail detail1 = new ContractDetail("Paulo Paula", 198756748L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail2 = new ContractDetail("Paulo Paula", 198756748L,"07/06/2017", 38, "23/05/2026");
        ContractDetail detail3 = new ContractDetail("Paulo Paula", 198756748L,"12/03/2017", 383, "23/05/2026");
        ContractDetail detail4 = new ContractDetail("Paulo Paula", 198756748L,"23/11/2012", 145, "23/05/2026");
        ContractDetail detail5 = new ContractDetail("Paulo Paula", 198756748L,"10/08/2015", 120, "23/05/2026");
        ContractDetail detail6 = new ContractDetail("Paulo Paula", 198756748L,"23/02/2019", 124, "23/05/2026");
        ContractDetail detail7 = new ContractDetail("Paulo Paula", 198756748L,"22/10/2017", 22, "21/05/2026");
        ContractDetail detail8 = new ContractDetail("Paulo Paula", 198756748L,"26/05/2017", 2, "16/05/2026");
        ContractDetail detail9 = new ContractDetail("Paulo Paula", 198756748L,"23/03/2017", 245, "21/05/2026");
        ContractDetail detail10 = new ContractDetail("Paulo Paula", 198756748L,"23/05/2018", 211, "23/11/2026");
        ContractDetail detail11 = new ContractDetail("João Santos", 565748930L,"23/05/2017", 566, "23/05/2026");
        ContractDetail detail12 = new ContractDetail("João Santos", 565748930L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail13 = new ContractDetail("João Santos", 565748930L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail14 = new ContractDetail("João Santos", 565748930L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail15 = new ContractDetail("Raquel Lopes", 343523456L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail16 = new ContractDetail("Raquel Lopes", 343523456L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail17 = new ContractDetail("Raquel Lopes", 343523456L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail18 = new ContractDetail("Patrícia Ferreira", 348948930L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail19 = new ContractDetail("Patrícia Ferreira", 348948930L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail20 = new ContractDetail("Luís Pereira", 565746356L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail21 = new ContractDetail("Luís Pereira", 565746356L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail22 = new ContractDetail("Samuel Santos", 565436830L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail23 = new ContractDetail("Samuel Santos", 565436830L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail24= new ContractDetail("Gonçalo Barros", 562647930L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail25 = new ContractDetail("Gonçalo Barros", 562647930L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail26 = new ContractDetail("Eva Domingues", 279048930L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail27 = new ContractDetail("Eva Domingues", 279048930L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail28 = new ContractDetail("Miguel Lisboa", 435648930L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail29 = new ContractDetail("Miguel Lisboa", 435648930L,"23/05/2017", 238, "23/05/2026");
        ContractDetail detail30 = new ContractDetail("Miguel Lisboa", 435648930L,"23/05/2017", 238, "23/05/2026");
        details.add(detail1);
        details.add(detail2);
        details.add(detail3);
        details.add(detail4);
        details.add(detail5);
        details.add(detail6);
        details.add(detail7);
        details.add(detail8);
        details.add(detail9);
        details.add(detail10);
        details.add(detail11);
        details.add(detail12);
        details.add(detail13);
        details.add(detail14);
        details.add(detail15);
        details.add(detail16);
        details.add(detail17);
        details.add(detail18);
        details.add(detail19);
        details.add(detail20);
        details.add(detail21);
        details.add(detail22);
        details.add(detail23);
        details.add(detail24);
        details.add(detail25);
        details.add(detail26);
        details.add(detail27);
        details.add(detail28);
        details.add(detail29);
        details.add(detail30);
        return details;
    }

}
