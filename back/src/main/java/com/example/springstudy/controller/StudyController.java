package com.example.springstudy.controller;

import com.example.springstudy.service.StudyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class StudyController {

    @Autowired
    private StudyService service;

    @GetMapping("/clients")
    public ResponseEntity<?> getClients() {
        return new ResponseEntity<>(this.service.getAllClients(), HttpStatus.OK);
    }

    @GetMapping("/detail")
    public ResponseEntity<?> getContractDetailsByName(@RequestParam String name) {
        return new ResponseEntity<>(this.service.getContractDetailsByName(name), HttpStatus.OK);
    }

}
