export interface Client {
  name: string;
  age: number;
  dateOfBirth: string;
  clientPhoneNumber: number;
}
