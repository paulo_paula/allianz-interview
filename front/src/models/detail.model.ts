export interface ContractDetail {
  clientName: string;
  clientPhoneNumber: number;
  contractDate: string;
  value: number;
  endingDate: string;
}
