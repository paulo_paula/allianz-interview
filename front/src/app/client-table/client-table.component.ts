import { Component, OnInit } from '@angular/core';

import { Client } from '../../models/client.model';
import { ClientService } from './service/client.service';

@Component({
  selector: 'app-client-table',
  templateUrl: './client-table.component.html',
  styleUrls: ['./client-table.component.css'],
})
export class ClientTableComponent implements OnInit {
  public clients: Client[] | undefined;
  displayedColumns: string[] = [
    'name',
    'age',
    'dateOfBirth',
    'clientPhoneNumber',
    'detail',
  ];

  constructor(private readonly clientService: ClientService) {}

  ngOnInit(): void {
    this.clientService
      .getClients()
      .subscribe((clients: Client[]) => (this.clients = clients));
  }

  public openModule(): void {
    console.log('Hello button click');
  }
}
