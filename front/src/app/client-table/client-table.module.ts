import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';

import { ClientDetailComponent } from '../client-detail/client-detail.component';
import { ClientTableComponent } from './client-table.component';
import { ClientTableRoutingModule } from './client-table.routing.module';

@NgModule({
  declarations: [ClientTableComponent, ClientDetailComponent],
  imports: [CommonModule, ClientTableRoutingModule, MatTableModule],
})
export class ClientTableModule {}
