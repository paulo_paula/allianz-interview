import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClientDetailComponent } from '../client-detail/client-detail.component';
import { ClientTableComponent } from './client-table.component';

const routes: Routes = [
  {
    path: '',
    component: ClientTableComponent,
  },
  {
    path: 'detail',
    component: ClientDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientTableRoutingModule {}
