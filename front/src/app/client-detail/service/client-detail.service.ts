import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ContractDetail } from '../../../models/detail.model';

@Injectable({
  providedIn: 'root',
})
export class ClientDetailService {
  constructor(private readonly http: HttpClient) {}

  public getClientDetail(param: string): Observable<ContractDetail[]> {
    let queryParams = new HttpParams();
    queryParams = queryParams.append('name', param);
    return this.http.get<ContractDetail[]>('http://localhost:8080/api/detail', {
      params: queryParams,
    });
  }
}
