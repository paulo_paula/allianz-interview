import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ContractDetail } from '../../models/detail.model';
import { ClientDetailService } from './service/client-detail.service';

@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.css'],
})
export class ClientDetailComponent {
  public details: ContractDetail[] | undefined;
  displayedColumns: string[] = [
    'clientName',
    'clientPhoneNumber',
    'contractDate',
    'clientPhoneNumber',
    'value',
    'endingDate',
  ];

  constructor(
    private route: ActivatedRoute,
    private readonly clientDetailService: ClientDetailService
  ) {}
  ngOnInit(): void {
    let queryParam: string = '';
    this.route.queryParams.subscribe((params) => (queryParam = params.name));
    this.clientDetailService
      .getClientDetail(queryParam)
      .subscribe(
        (contractDetails: ContractDetail[]) => (this.details = contractDetails)
      );
  }
}
